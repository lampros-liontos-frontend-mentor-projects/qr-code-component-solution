# Frontend Mentor - QR code component solution

This is a solution to the [QR code component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/qr-code-component-iux_sIO_H). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)

**Note: Delete this note and update the table of contents based on what sections you keep.**

## Overview

This is simply a box on a page showing the QR code for accessing Frontend Mentor's main site.  It should be visible on desktops, laptops, and mobile devices without issue.

### Screenshot

![](./screenshot.png)

### Links

- Solution URL: [GitLab Project Page](https://gitlab.com/lampros-liontos-frontend-mentor-projects/qr-code-component-solution)
- Live Site URL: [Live Netlify App Site](https://master--ll-fm-qr-code-component-solution.netlify.app/)

## My process

### HTML File

I began by rewriting the HTML to start from scratch.  I removed the CSS from the HTML, and placed it in its own file, `style.css`.  After this, I added a `viewport` meta to ensure that the positioning and size code would benefit from information about the hosting device... or the overall shape of the window.  This will keep the QR-code in the center of the window.  I then added `div` elements for each section of the page, including the main box, the "call to action" section, and the attribution.  Everything I planned to modify, I gave `class` attributes to, so that they can be edited from the CSS file.

### CSS File

Once I finished with the HTML, I went to work in the CSS file.

First, I assigned a color to the body of the page.  This will provide the backdrop color that will allow the content box to stand out.  Then I chose straight white for the content box.  Once the colors were assigned, I then added curved corners on the image and the content box.

Now that the box stands out from the background, I shrank it down, and centered it on the body using a flexbox.  Because of the previously-mentioned `viewport`, I was able to assign the size of the body to be 98% of the window's total space (to prevent scrollbars from appearing).  Once the body of the message had a specified size, I could then use the flexbox to center the content box.

Finally, once the flexbox was set, and the content box's design was correct, it was just a matter of adjusting the font sizes to match the way the text was displayed.  I included a stack of fonts, just in case the "Outfit" font was inaccessible, or someone has their browser configured to ignore external fonts.  This ensures that a sans-serif font is used, keeping the appearance consistent.

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- Mobile-first workflow

### What I learned

I knew a number of things already, but there were some things I learned from this process.  After some study to figure out why I was unable to vertically center the box, I learned that, in order for a box to be centered vertically, the box has to be within a fluxbox container, and that the container needs to be large enough for the box to be centered *in*.  This means that I had to adjust the size of the page to closely match the size of the browser window... and with `viewport`, this meant using the `98vh` and `98vw` size attributes, which is essentially 98% of the window's height and 98% of the window's width, respectively.  Once this was done, the `justify content:center` and `align-items: center` attributes worked as desired.

Another *very* important thing I learned was the setup of CI/CD using GitLab.  I learned that most of the work is done through the `.gitlab-ci.yml` file, and I learned how that file is populated.  Additionally, since my hosting only provides FTP access, I spent some time learning how to use the `lftp` program in a docker image to upload the appropriate files to my hosting, so that it can be accessible.

### Continued development

There's not much here to expand upon, even though I did benefit from the size/centering issue mentioned above.  However, more projects await, and I certainly look forward to seeing how they work out.

One thing I could do is get some code in here to make this into a generated item, so that other web pages can call on this page with custom data to include on the QC code, instead of just showing a static image.

### Useful resources

- [W3Schools](https://www.w3schools.com) - This is an excellent resource for basic information about features of multiple languages, including CSS and HTML.
- [DevDocs](https://devdocs.io/) - While W3Schools is good at giving you instruction in various languages, including CSS and HTML, DevDocs is an incredible reference utility to perform lookups for more detailed information.  You could get lost in a "wiki binge" going through this site!
- [ChatGPT](https://chatgpt.com) - I know that a lot of people dislike this tool, I find it makes an excellent reference platform.  Admittedly, it has some serious flaws, and I don't plan to let it actually touch my code, but it makes an excellent starting reference, so that I'm spending more time confirming what I've learned, than casting about in the dark.

## Author

- Website - [Lampros Liontos](https://www.lamprosliontos.com)
- Frontend Mentor - [@reteo](https://www.frontendmentor.io/profile/yourusername)
- Twitter - [@reteov](https://www.twitter.com/reteov)
